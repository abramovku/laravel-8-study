<html>

<head>
    <title>Blog</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
            integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>
    <script src="{{ asset('/js/app.js')}}"></script>
    <script>
        const article_wrapper = 'article-list';
        window.Echo.channel('general')
            .listen('ArticleAddedEvent', function (e) {
                console.log(e);
                let article = "<div class='col-lg-12 margin-tb' id='article_" + e.id + "'>";
                    article += "<div class='title'><h2><a href='/page/article/" + e.id + "'>" + e.title
                        + "</a></h2></div>";

                    article += "<div class='date'><strong>Created at:</strong>" + e.created_at + "</div>";
                    article += "<div class=\"content\">" + e.content + "</div>";
                    article += "</div>";

                document.getElementsByClassName(article_wrapper)[0].innerHTML += article;
            })
            .listen('ArticleDeleteEvent', function (e) {
                console.log(e);
                document.getElementById('article_' + e.id).remove();
            });
    </script>
</head>

<body>
<div class="container">
    @yield('content')
</div>
</body>

</html>
