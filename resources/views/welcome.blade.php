@extends('layouts.app')

@section('content')
    <div class="row article-list">
        @foreach ($articles as $article)
            <div class="col-lg-12 margin-tb" id="article_{{ $article->id }}">
                <div class="title"><h2><a href="/page/article/{{ $article->id }}">{{ $article->title }}</a></h2></div>
                <div class="category">{{ $article->category ? $article->category->title : '' }}</div>
                <div class="date"><strong>Created at:</strong> {{ $article->created_at }}</div>
                <div class="content">{{ \Illuminate\Support\Str::limit($article->content, 20, ' (...)') }}</div>
                <div class="author"><strong>Author:</strong> {{ $article->user->name }}</div>
            </div>
        @endforeach
    </div>
@endsection
