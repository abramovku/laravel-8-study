@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="title"><h2>{{ $article->title }}</h2></div>
            <div class="category">{{ $article->category ? $article->category->title : '' }}</div>
            <div class="date"><strong>Created at:</strong> {{ $article->created_at }}</div>
            <div class="content">{{ $article->content }}</div>
            <div class="author"><strong>Author:</strong> {{ $article->user->name }}</div>
        </div>
    </div>

    <div class="row comments-list" @unless($article->comments) style="display: none;" @endunless id="commentList">
        <h1>Comments: </h1>
        @foreach ($article->comments as $comment)
        <div class="col-lg-12 margin-tb">
            <div class="title"><h3>{{$comment->title}}</h3></div>
            <div class="date"><strong>Created at:</strong> {{ $comment->created_at }}</div>
            <div class="content">{{ $comment->content }}</div>
            <hr>
            <br>
        </div>
        @endforeach
    </div>

    @auth
    <div class="row comments-form">
        <div class="col-lg-12 margin-tb">
            <form action="{{ route('comment.store') }}" method="POST" id="commentForm">
                @csrf
                <input type="hidden" name="article_id" value="{{$article->id}}"/>
                <input type="text" name="title" value="" placeholder="title*"/>
                <textarea name="content" placeholder="comment"></textarea>
                <button type="button" id="submit_comment">leave comment</button>
            </form>
        </div>
    </div>
    @endauth

    @guest
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <a class="btn btn-primary" href="/login">Please login!</a>
            </div>
        </div>
    @endguest

    <script>
        window.Echo.private('comment.{{$article->title}}')
            .listen('CommentAddedEvent', function (e) {
                console.log(e);
                const comment_wrapper = 'commentList';
                let commentTemplate = "<div class='col-lg-12 margin-tb'>";
                commentTemplate += "<div class=\"title\"><h3>" + e.data.title + "</h3></div>";
                commentTemplate += "<div class=\"date\"><strong>Created at:</strong>"
                    + e.data.created_at + "</div>";
                commentTemplate += "<div class=\"content\">" + e.data.content + "</div><hr><br></div>";

                document.getElementById(comment_wrapper).innerHTML += commentTemplate;
            });
    </script>
@endsection
