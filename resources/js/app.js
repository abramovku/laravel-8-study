require('./bootstrap');

$(document).ready(function () {
    $('#submit_comment').on('click',function (event) {
        event.preventDefault();
        let formPath = $('#commentForm');
        let ListPath = $('#commentList');
        let path = formPath.attr('action');
        let formData = formPath.serialize();

        axios.post(path, formData)
            .then(function (response) {
                formPath[0].reset();
                let commentTemplate = "<div class='col-lg-12 margin-tb'>";
                commentTemplate += "<div class=\"title\"><h3>" + response.data.title + "</h3></div>";
                commentTemplate += "<div class=\"date\"><strong>Created at:</strong>"
                    + response.data.created_at + "</div>";
                commentTemplate += "<div class=\"content\">" + response.data.content + "</div><hr><br></div>";
                console.log(response);
                ListPath.append(commentTemplate);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    });
});
