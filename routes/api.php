<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('api')->group(function() {
    Route::post('register', [\App\Http\Controllers\api\AuthController::class, 'register']);
    Route::post('token', [\App\Http\Controllers\api\AuthController::class,'token']);
});

Route::middleware(['auth:sanctum'])->post('/name', function (Request $request) {
    return response()->json(['name' => $request->user()->name]);
});

Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return ['token' => $token->plainTextToken];
});
