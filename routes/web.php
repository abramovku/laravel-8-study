<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PageController::class, 'index']);
Route::get('/page/article/{article}', [\App\Http\Controllers\PageController::class, 'article']);
Route::middleware(['auth:web'])->group(function () {
    Route::get('home', [\App\Http\Controllers\PageController::class, 'home'])->name('home');
    Route::resource('article', \App\Http\Controllers\ArticleController::class);
    Route::resource('category', \App\Http\Controllers\CategoryController::class);
    Route::resource('comment', \App\Http\Controllers\CommentController::class);
});
