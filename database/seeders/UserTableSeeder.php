<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@blog.com',
                'password' => Hash::make('adminadmin'),
            ],
        ];

        foreach ($users as $item) {
            \App\Models\User::create([
                'name' => $item['name'],
                'email' => $item['email'],
                'password' => $item['password']
            ]);
        }
    }
}
