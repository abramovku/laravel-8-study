<?php

namespace App\Listeners;

use App\Events\ArticleHasViewed;

class ArticleHasViewedListener extends Listener
{
    public function handle(ArticleHasViewed $event)
    {
        //
    }
}
