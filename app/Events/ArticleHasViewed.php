<?php

namespace App\Events;

use App\Models\Article;

class ArticleHasViewed extends Event
{
    /**
     * ArticleHasViewed constructor.
     * @param Article $article
     * @param null $ip
     * @param null $user_id
     */
    public function __construct(Article $article, $ip = null, $user_id = null)
    {
        //
    }
}
