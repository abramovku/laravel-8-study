<?php

namespace App\Services\View;

use Illuminate\Filesystem\Filesystem;

class FileViewFinder extends \Illuminate\View\FileViewFinder
{
    private $theme;

    public function __construct(Filesystem $files, array $paths, array $extensions = null, $theme = null)
    {
        parent::__construct($files, $paths, null);
        $this->theme = $theme;
    }

    protected function findInPaths($name, $paths)
    {
        foreach ((array) $paths as $path) {
            foreach ($this->getPossibleViewFiles($name) as $file) {
                if (!empty($this->theme)) {
                    if ($this->files->exists($viewPath = $path . '/' . $this->theme . '/' . $file)) {
                        return $viewPath;
                    }

                    if ($this->files->exists($viewPath = $path . '/default/' . $file)) {
                        return $viewPath;
                    }

                    if ($this->files->exists($viewPath = $path . '/' . $file)) {
                        return $viewPath;
                    }
                }

                if ($this->files->exists($viewPath = $path . '/' . $file)) {
                    return $viewPath;
                }
            }
        }

        throw new \InvalidArgumentException("View [{$name}] not found.");
    }
}
