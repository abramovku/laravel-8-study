<?php

namespace App\Console\Commands;

use \Illuminate\Database\Console\Migrations\MigrateCommand;

class DataMigrateCommand extends MigrateCommand
{
    protected $signature = 'data_migrate {--database= : The database connection to use}
                {--force : Force the operation to run when in production}
                {--path=* : The path(s) to the migrations files to be executed}
                {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths}
                {--schema-path= : The path to a schema dump file}
                {--pretend : Dump the SQL queries that would be run}
                {--seed : Indicates if the seed task should be re-run}
                {--step : Force the migrations to be run so they can be rolled back individually}';

    protected $description = 'Run the database data migrations';

    protected function getMigrationPath()
    {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'data_migrations';
    }
}
