<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\ApiGetTokenRequest;
use App\Http\Requests\ApiRegistrationRequest;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function token(ApiGetTokenRequest $request)
    {
        $requestData = $request->validated();
        $user = User::where('email', $requestData['email'])->first();

        if (!$user || !Hash::check($requestData['password'], $user->password)) {
            return response()->json(['error' => 'The provided credentials are incorrect.'], 401);
        }

        return response()->json(['token' => $user->createToken($requestData['device_name'])->plainTextToken]);
    }

    public function register(ApiRegistrationRequest $request)
    {
        $requestData = $request->validated();

        $requestData['password'] = bcrypt($requestData['password']);
        $user = User::create($requestData);
        $token = $user->createToken($requestData['device_name'])->plainTextToken;

        return response()->json(['token' => $token], 200);
    }
}
