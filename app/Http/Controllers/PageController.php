<?php

namespace App\Http\Controllers;

use App\Events\ArticleHasViewed;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $articles = Article::with('user')->get();
        return view('welcome', compact('articles'));
    }

    public function home()
    {
        $user = Auth::user();
        $articles = Article::where('user_id', $user->id)->with('categories')->get();
        return view('article.index', compact('articles'));
    }

    public function article(Article $article, Request $request)
    {
        event(new ArticleHasViewed($article, $request->ip(), Auth::id()));
        return view('article', compact('article'));
    }
}
