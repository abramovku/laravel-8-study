<?php

namespace App\Http\Controllers;

use App\Events\Broadcasting\ArticleAddedEvent;
use App\Events\Broadcasting\ArticleDeleteEvent;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $articles = Article::where('user_id', $user->id)->with('categories')->get();
        return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
        ]);

        $user = Auth::user();

        $data = array_merge(['user_id' => $user->id], $request->all());

        $article = Article::create($data);

        $data = array_merge(['id' => $article->id, 'created_at' => $article->created_at], $data);

        if (!empty($request->category)) {
            $category = Category::find($request->category);
            $article->categories()->attach($category);
        }

        broadcast(new ArticleAddedEvent($data));

        return redirect()->route('article.index')
            ->with('success', 'Article created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $user = Auth::user();
        if ($user->cannot('update', $article)) {
            abort(403);
        }
        $categories = Category::all();
        return view('article.edit', compact(['article', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required'
        ]);
        $article->update($request->all());

        if (!empty($request->category)) {
            $category = Category::find($request->category);
            $article->categories()->attach($category);
        }

        return redirect()->route('article.index')
            ->with('success', 'Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        broadcast(new ArticleDeleteEvent($article->id));

        return redirect()->route('article.index')
            ->with('success', 'Article deleted successfully');
    }
}
