<?php

namespace App\Providers;

use App\Console\Commands\DataMigrateCommand;
use App\Services\View\FileViewFinder;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\Migrations\MigrationRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('view.finder',
            function ($app) {
                return new FileViewFinder($app['files'],
                    $app['config']['view.paths'],
                    null,
                    $app['config']['app.view_theme']
                );
            }
        );

        $this->app->bindIf(MigrationRepositoryInterface::class, 'migration.repository');

        $this->app->when([DataMigrateCommand::class])
            ->needs(\Illuminate\Database\Migrations\Migrator::class)
            ->give('data-migrator');

        $this->app->bind('data-migrator', function ($app) {
            $table_data = $app['config']['database.data_migrations'];
            $repository = new DatabaseMigrationRepository($app['db'], $table_data);
            return new Migrator($repository, $app['db'], $app['files'], $app['events']);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
